# Polyamide-12 Laser Sintering Overview

Polyamide-12 (PA-12) is a semi-crystalline polymer which can be sintered by raising it's temperature above its melting point to induce a rapid phase change in a localized area. This high temperature dependence is a characteristic of semi-crystalline polymers, which experience changes in their properties as their temperature rises above key points, mainly:

- $`T_c`$, the crystalization temperature
- $`T_g`$, the glass transition temperature
- $`T_m`$, the melting temperature

The main advantage of LS over other 3D printing methods is the ability to print objects that can otherwise not be produced with subtractive manufacturing methods (machining) or current additive methods (fused deposition methods, i.e. traditional 3D printers).

This page is intended to provide a high-level overview of the LS process and the major factors that must be considered when designing an LS system.



## Material Profile

PA-12 has the following important characteristics that make it a good candidate for sintering

- narrow melting temperature window

![DSC plot for PA-12](img/PA_12_DSC_Kruth_Levy.png)

As seen in this differential scanning calorimetry (DSC) plot, PA-12 has a sharp melting peak corresponding to a narrow temperature range over which the melting phase change occurs.

![Terms used to define the melt region of PA-12](img/PA_12_DSC_Terms_Duddleston_Thesis.png)

![Effect of molecular weight on ploymer melting](/img/MW_Effect_on_LS_Duddleston_Thesis.png)

https://en.wikipedia.org/wiki/Glass_transition#Transition_temperature_Tg

For practical purposes, polymer powders such as PA-12 are often discussed in terms of spherulites, which are spherical structures composed of polymer chains, rather than evaluating them in terms of the polymer chains themselves. During the LS process, these spherulites are heated by the laser and begin to melt, forming "necks" between other adjacent spherulites; this is the main method by which sintering consolidates material to produce solid parts. Spherulite diameter of 45 um to 90 um is recommended for sintering, 50 um is a commonly used size in the current literature.

![Illustration showing spherulite necking](img/Particle_Necking_Goodridge.png)

https://en.wikipedia.org/wiki/Crystallization_of_polymers

https://en.wikipedia.org/wiki/Spherulite_(polymer_physics)

Final part quality can roughly be corrolated linearly with the density (i.e. the degree of material consolidation) of the produced part.

- higher density => better mechanical properties



Currently, sintered parts will always contain some unsintered material at the centre of the spherulites, compared to injection-molded parts which fully melt the polymer material used to produce them.





## Designing a LS printer

There are many factors that affect the design of a LS printer. The list below outlines all the sigificant factors currently identified, what printer systems they are associated with, how they affect the final print quality, possible mitigations, and what other factors (if any) they are interrelated with. 



### Atmosphere control
---

**Moisture content**  
In the build chamber atmosphere, required level is <30%, for better/best results need to reduce to <10%

**Oxygen content**  
PA-12 is sensitive to thermal oxidative degredation. This needs to be < 5%, suggest a target concentration of 0.5% (by volume)

**Build chamber pressure**  
Sintering is performed at 1 atm

**Gas dispersion method**  
Inert atmosphere creation can be done prior to any powder deposition thus there may be no need  to reduce currents that can dislodge powder. Given that N\_2(g) is lighter than air, placing the input valves at the top of the chamber and the outtake valves at the bottom should allow the Nitrogen gas to push the heavier O\_2 out the bottom as the chamber fills. This also makes the bottom of the build chamber a natural place to put the O\_2 sensors as it will contain the highedt concentration of Oxygen (thus if the oxygen content in this area is brought within the deisred range, the rest of the build chamber would also be within that range or lower).



### Thermal management
---

**Thermal gradients**  
Minimizing these is essential to maintaining print accuracy. Thermal gradients will exist between:

- layers in the print volume
- the print volume and the print bed
- the print volume and its surrounding walls
- the working layer and the atmosphere
- sintered and unsintered powder in the working layer

**Pre-heat temperature**  
The bulk material (powder) needs to be heated up to approx 170 $`^\circ`$C before being sintered by the laser. 

**Heating rate**  
The bulk unsintered material needs to be heated up to/close to the pre-heat temperature before being sintered

**Cooling rate**  
After final layer, parts need to cool slowly to reduce warpage/shrinkage. 1 $`^\circ`$C/min is a reasonably slow cooling rate

**Target melting temperature**  
190 $`^\circ`$C



### Powder dispersal
---

**Powder layer surface uniformity**  
After spreading the working layer surface should be planar. Based on previous research and other known implementations, a counter-rotating roller should be optimal solution.

**Layer thickness**  
Ideally 0.1 - 0.15 mm

**Additives**  
in the powder (e.g. carbon black), these increase absorptivity but can cause localized heating effects that increase part porosity (associated with lower cracking threshold). 

**Refresh rate**  
(fraction of powder re-used from previous builds). Can range from 30% to 70% depending on manufacturer recommendation, higher refresh rate means powder will have experienced fewer thermal cycles which is associated with a molecular weight distribution that is skewed lower and less agglomeration of particles. Print quality decreases with higher powder re-use (lower refresh rate).

**Powder melt flow rate ** 
No easy way to quantify this. Potential for future work but not able to incorporate information derived from this method into system requirements.

**Average molecular weight**  
Practically speaking, polymers never have a particles with a single well-defined molecular weight (due to chain scission, incomplete polymerization, etc). Instead, polymers have a distribution of molecular weights centered around the most common molecular weight present (e.g. a molecule with 12 repeat units, in the case of PA-12). 

**Powder compression/compaction**  
Compression of the powder is undesirable as it can cause non-uniform layer height when depositing a new layer of powder and also increases the potential for shifting or dislodging the previously sintered material. It should be avoided wherever possible.



### Laser system
---

**Laser power**  
Ideally <=10 W (as in the FUSE-1). A higher pre-heat temperature reduces the required laser power.

**Scan spot diameter**  
Larger diameter can improve part quality by providing a more even irradiation of the target area, at the cost of increased laser power.

**Laser wavelength**  
1064 nm. This allows the use of a Nd:YAG laser which can be routed through optical fiber. 
https://en.wikipedia.org/wiki/Nd:YAG_laser

**TEM mode**  
TEM_00  is the preferred mode for the laser to operate in as it provides a Gaussian energy distribution centred at the middle of the laser focal point (this gives easily predictable energy density for different focal legnths/spot sizes compared to higher order TEM modes). 
https://en.wikipedia.org/wiki/Transverse_mode



### Print geometry and layout
---

**Part orientation**  
Final part properties in LS are generally anisotropic. The inter-layer particle necks are generally weaker than the intra-layer necks, due to the higher temperature difference between layers as opposed to within them.

**Shrinkage**  
All parts produced via LS experience shrinkage when cooling, thus designs must be scaled by an empirically determined scaling factor

**Scanning order**  
When tracing out the part cross-section, the contour and fill can be scanned separately. This affects the final surface finish and the generation of thermal gradients within the working layer.

**Sinterscanning**  
A technique which swaps the x- and y-axis in each successive layer when printing. This can reduce the anisotropy of produced parts (yields more uniform mechanical properties in the finished parts).

**Scan speed**  
The speed at which the laser focal point moves when scanning a part layer. Depending on the desired rate, a higher or lower laser power is required (if moving faster or slower, respectively)

**Multi-pass scanning**  
A strategy where each trace/path in the working layer is scanned (irradiated) multiple times. This gives the particles in the scan path multiple exposures to the laser, which increased the average energy absorption of the particles without requiring an increase in laser power (useful in reducing flash heating of the powder).

**Delay time**  
Between scanning successive points in the part cross section, increased delay time allows thermal gradients to develop and decreases final part quality. This needs to be considered for movement in both the x- and y-directions.
