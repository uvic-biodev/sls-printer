# Thermodynamic model of SLS print process

### Contents

[Overview](#Overview)  
[Material Properties](#Material Properties)  
[Mathematical Model](#Mathematical-Model)  
[References](#References)  



## Overview

The Selective Laser Sintering (SLS) process is a method by which a powdered material is fused (sintered) using a laser which is incident on a layer of powder and traces a desired path . When repeated many times with subsequent layers placed on top of the previous layer, this process can be used to construct three-dimensional objects similar to traditional fused deposition model 3D printers.

Advantages

- powder acts as a support structure during part fabrication
- no clogging of print nozzle
- produces strong, accurate parts 
- allows for printing of complex geometric shapes


Disadvantages

- use of high-powered laser requires extra safety precautions as well as built in redundancies and failsafes
- powder movement, storage, and deposition methods must adhear to safe handling proceedures depending on the type of powder used



## Material Properties

The following tables summarize important material parameters for Polyamide 12 (PA12)


| PA12                                   | Value | Units                      |
| -------------------------------------- | ----- | -------------------------- |
| Specific heat capacity, $`c_p`$          | 1.17 - 1.2	| Kj/$`{kg\ {^\circ}K}`$ |
| Thermal conductivity (powder), $`k_D`$   | 0.24  		| W/$`{m\ {^\circ}K}`$   |
| Thermal conductivity (sintered), $`k_S`$ | 0.24  		| W/$`{m\ {^\circ}K}`$   |
| Glass transition temperature, $`T_G`$    |       		| $`^{\circ}C`$     |
| Melting temperature, $`T_m`$             | 180\*  | $`^{\circ}C`$         |
| Density (powder), $`\rho_D`$             | 0.46 - 0.48 | g/cm^3           |
| Density (sintered), $`\rho_S`$           | 0.95 - 1.0\*  | g/cm^3         |
|                                          |       		|                   |

\*entries marked with a "\*" denote estimated values. See http://sffsymposium.engr.utexas.edu/Manuscripts/1990/1990-19-Xue.pdf (replace with reference code) for details on powder/sintered property comparisons

http://www.prodways.com/en/material/pa12-l-1600/

https://www.matbase.com/material-categories/natural-and-synthetic-polymers/thermoplastics/engineering-polymers/material-properties-of-polyamide-12-nylon-12-pa-12.html#properties


## Mathematical Model

The thermodynamic processes involved in sintering the powdered material are modelled as follows. The printing area is divided into two layers: the working layer (where sintering takes place), and the sublayer (the layer that was previously sintered). 

![SLS model](/img/Diagrams.png)

*Replace above diagrams with TikZ ($`L^{A}T_{E}X`$ programmatic) based versions

This is done to account for differences between the layers; some important properties that differ between the layers are:

- bulk material temperature
- type and amount of material adjacent to the working volume


In addition, the thermal conductivity of the sublayer depends on the position of the working volume in the working layer (i.e. whether the working volume is over powder or sintered material).


The table below lists the parameters and important relations used to model the SLS process.

| Model element                            | Symbol or Formula                        | Units |
| ---------------------------------------- | ---------------------------------------- | ----- |
| Working volume diameter (laser focal diameter) | $`d`$                                      | mm    |
| Working layer height                     | $`l`$                                      | mm    |
| working volume                           | $`V = A_c l`$                              | mm^3  |
| mass                                     | $`m =\rho V`$                              | g     |
| working volume internal energy change    | $`\Delta U = c_p m(T_m - T_l)`$            | kJ    |
| working volume enthalpy change (important?) | $`\Delta H = \Delta U + p\Delta V`$     | kJ    |
| Working volume layer powder area ratio   | $`\alpha = \frac{A_p}{A_b}\ where\ \alpha \in [0,1]`$ | N/a   |
| Working volume sublayer powder area ratio | $`\beta = \frac{A_p}{A_c}\ where\ \beta \in [0,1]`$ | N/a   |
| Focal area                               | $`A_c =\pi(\frac{d}{2})^2`$                | mm^2  |
| Boundary area                            | $`A_b = \pi dl`$                           | mm^2  |
| Heat loss rate (working layer)           | $`Q_b = [\alpha q_{b,1}'' + (1-\alpha)q_{b,2}'']A_b`$ | W     |
| Heat loss rate (sublayer)                | $`Q_c = [\beta q_{c,1}'' +(1-\beta)q_{c,2}'']A_c`$ | W     |
| Heat loss by conduction (Fourier's law)  | $`q_x''= \frac{k}{L}(T_{s,1} - T_{s,2})`$  | W/m^2 |
| Feed rate                                | $`\Gamma`$ (tenative)                      | mm/s  |
| Volume exposure time                     | $`t_{ve} = \frac{d}{\Gamma}`$              | s     |
| Required power delivery                  | $`Q_R = \frac{\Delta U}{t_{ve}}`$          | W     |
|                                          |                                          |       |


$`G = G_{abs} + G_r + G_t`$, 

where $`G_r`$ is the reflected radiation, $`G_t`$ is the transmitted radiation, $`G`$ is the irradiation directed towards the working volume surface

$`G_{abs} = Q_b + Q_c + Q_R +Q_0`$ , 

where $`Q_0`$ is a term encompassing other working volume losses not due to conduction or heating of the working volume



**notes**

- may need to consider sublayer material (sintered/powder) when sintering woking layer, laser power adjusting might be needed to keep desired thermal conduction profile depending on what type of material the working volume is adjacent to
- essentially the goal is to keep the material temperature at or near the glass temperature to ensure good bonding when adjacent to other sintered material, but to also use the thermal contact resistance (via insulating air pockets) in the powder to maintain a sharp boundary when the working volume is *not* adjacent to sintered material (to maintain print resolution and accuracy)



## References

https://en.wikipedia.org/wiki/Sintering#Plastics_sintering

http://sffsymposium.engr.utexas.edu/Manuscripts/1990/1990-19-Xue.pdf
