% Preamble
% ////////////////////////////////////////////////////////////////////////////////////////////
%	GOAL: To create a generalized analysis script that can determine the power required for SLS
%	of a given powder, based on it's physical properties and a consistent model of the SLS process.
% ////////////////////////////////////////////////////////////////////////////////////////////

% clear workspace
clear all;
clc;

% print parameters
% ////////////////////////////////////////////////////////////////////////////////////////////

feed_rate = 100;	% speed of print head moving along working layer [mm/s]
chamber_temp = 100;	% temperature the chamber is heated to [C]
T_wl = 170;			% working layer bulk material temperature
T_sl = 166;			% sublayer bulk material temperature
wavelength = 10.6; 	% laser wavelength in micrometers

% desired target temperature to bring working volume to for sintering
%	target temp ~= glass temp, depends on how far into the glass phase material should be heated
% T_tgt = 190;

% powder properties
% ////////////////////////////////////////////////////////////////////////////////////////////

rho = 1.02;		% powder density [g/cm^3]
c_p = 1.2;		% specific heat (constant pressure) of powder [kJ/kg*K]
T_m = 190;		% melting point temperature of PA12 powder
T_g = 80;		% the temperature at which the glass transition takes place for the powder [C]
k_po = 0.24;	% thermal conductivity of powder [mW/mm*K]
k_pl = 0.24;	% thermal conductivity of sintered material [mW/mm*K]
% powder absorptivity
% thermal expansion coefficients

% Powder model: cylinder with diameter d, height l
% ////////////////////////////////////////////////////////////////////////////////////////////
%	bounded on sides and bottom by sintered and unsintered powder
%	accounted for using ratios alpha and beta

d = 0.25;			% diameter of working volume [mm]
l = 0.25;			% height of working layer [mm]
A_c = pi*(d/2)^2;	% area of cylinder top and bottom
A_b = pi*d*l;		% area of cylinder side walls
a = 1;		% alpha, layer powder ratio. Range [0, 1]
			% The fraction of the working volume side boundary adjacent to unsintered powder
b = 1;		% beta, sublayer powder ratio. Range [0, 1]
			% The fraction of the working volume sublayer boundary adjacent to unsintered powder

% the minimum length to consider for conduction equations (powder)
% 	due to thermal contact resistance and gas pockets in powder
% 	this is likely a worst-case consideration. Quite possible that no or insufficient conduction 
%	will take place into adjacent powder to cause glass phase transition in adjacent powder
L_min_po = min(l, d);	% to keep consistent print accuracy specifications?

% the minimum length to consider for conduction equations (sintered material)
%	should be the minimum of the layer height and working volume diameter
%	material should not be in the glass phase at this length to avoid warping/deformation
L_min_pl = min(l,d);	% to keep consistent print accuracy specifications?


% derived relations
% ////////////////////////////////////////////////////////////////////////////////////////////

m = (rho/1000)*A_c*l; % mass of powder in working volume [g/mm^3][mm^3]

t_ve = d / feed_rate; % volume exposure time, time available to absorb energy from laser

% using q'' = (k/L)*(T_1 - T_2), T_1 > T_2
q_wl_po = (k_po/L_min_po)*(T_m - T_wl);	% heat flux into working layer powder
q_wl_pl = (K_pl/L_min_pl)*(T_m - T_wl);	% heat flux into working layer plastic
q_sl_po = (k_po/L_min_po)*(T_m - T_sl);	% heat flux into sublayer powder
q_sl_pl = (k_pl/L_min_pl)*(T_m - T_sl);	% heat flux into sublayer plastic

Q_b = (a*q_wl_po + (1-a)*q_wl_pl)*A_b;	% heat loss rate to working layer
Q_c = (b*q_sl_po + (1-b)*q_sl_pl)*A_c;	% heat loss rate to sublayer						

Q_R = (c_p*m*(T_m - T_wl))/t_ve; % required power absorption by the working volume to melt powder

% 
G_abs = Q_b + Q_c + Q_R;	% absorbed laser power = 
							%	power required for enthalpy change in WV
							%	+ heat losses by conduction to working layer and sublayer

G = G_abs + G_r + G_t + G_0; 	% total emmissve power of the laser G = 
								% 	laser power absorbed by the working volume G_abs
								% 	+ laser power reflected from the working volumr surface G_r
								% 	+ laser power transmitted through the working volume G_t
