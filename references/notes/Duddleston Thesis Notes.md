# Duddleston Thesis Notes

30% - 70% powder refresh rate is common, 

Recycling fo powder is not well understood at this time

*good thermoplastic diagram on pg. 5 (pg. 22 in pdf)

Polymer end groups affect crystallinity which affects melting (act as impurities)

most polymers are not monodisperse (i.e. they do not have uniform chain lengths)

- they have a distribution of chain sizes
- the shape of this distribution is related to the average molecular weight of the polymer, which in turn affects the melting point and other material properties

Evonik Degussa Gmbh is a PA12 supplier for much of the sintering industry

commercially available powders typlically have particle sizes of 10um to 1000um in diameter

the melting point of ploymers, t_m, onset, can be reduced by adding impurities

- understanding the melting process is key to SLS

DTM Corp. -> 1st commercial SLS machine

- thesis and analysis based on a DTM SinterStation 2500 machine
- results should apply generally to all PA12 SLS processes

part placement in build chamber must be determined such that it optimizez the powder conversion and minimizez thermal gradients in the material

PA12 is especially sensitive to thermal-oxidative degredation

- thus the need to inert the build chamber with Nitrogen gas (need to reduce O2 content to less than 5% by volume)

Preheating of powder

- feed bins -> 80 'C
- part chamber -> 166 - 170 'C
- surface layer -> 170 'C
- build time is approx 30 to 60 seconds/layer based on these preheat values

Build chamber needs to be cooled slowly (24 to 48 hours) to minimize part warping

density_of_powder ~= 0.5*density_of_sintered parts 

- i.e.  powder density is approx 0.46 g/cm^3

Section 2.5.1.5 has list of SLS advantages

Key factors that affect powder degredation

- Temperature
- O2 content
- molecular weight
- radiation
- crystal structure
- crystallinity

Differential Scanning Calorimetry  (DSC) gives the phase-vs-temperature plot for a polymer

- shows T_m, onset; T_m, peak; T_m, end

Increasing laser power when using recycled powder can imporve part quality (Since T_m, end increases with thermal degredation).

- potential powder correction forumla based on DSC readings P = a * P_0 * T_m, end / T_m, onset

SLS powder is approx $100.00/kg for commercial grade