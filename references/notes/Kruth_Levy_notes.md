Two important types of thermoplastics (polymers) for SLS

- Semi-crystalline thermoplastics (PA12 is one of these)
  - Highly ordered molecular structure
- amorphous thermoplastics
  - non-ordered molecular structure

Both change their properties when heated

- hard -> "soft" (tough leather/rubbery) -> flowing melt

Phase vs Temperature diagram (semi-crystalline polymers)

  Hard elastic (brittle)	   Hard elastic (tough)	    Viscous melt

<------------------------------|------------------------------|---------------------------->

​				      T_g				    T_m

T_g = glass transisition temperature (-100 C -> 50 C)

T_m = melting temperature (100 C -> 400 C)

- both T_g and T_m depende on the molecular weight of the polymer



Laser consolidation of semi-crystalline polymer powders occurs by heating the powder above the melting temperature

- semi-crystalline materials have sharp melting points (\Delta T_m is narrow, centred at T_m)
- rapid cooling of the consolidated material can cause volume changes and undesirable geometric inaccuracies and distortion
  - pre-heating the powder to a temperature close to T_m can reduce these effects



Powder re-use

- expect no more than 3 builds using the same powder before the mechanical properties of the prduced part are unacceptable
- atmosphere control in the build volume is *critical* to the SLS process
- Scanning strategies are needed to achieve full powder consilidation and to minimize contour inaccuracies
  - e.g. scan layer contours first, multi-pass scanning, scan overlapping