Thermal conductivity of PA12 powder
Thermal conductivity of sintered PA12
Density of PA12 powder
reflectivity of PA12 powder
transmisivity of PA12 powder
How do different silicone heat pads distribute heat across their surface?
Absorptivity of PA12 powder
    - What wavelength of light is best to use for the laser?
How fast can PA12 be heated/cooled without causing unwanted powder consolidation/warping?
Laser scanning strategies
    - What has been used in SLS?
    - What are the advantages/disadvantages of each strategy?
    - What hardware is required to implement a given strategy?